#!/bin/env python3

import re
import argparse
import sys
from output_printer import OutputPrinter


def compile_re_pattern(pattern):
    """
    compiles regex using re
    :param pattern: a regex pattern to compile
    :returns compiled re pattern
    """
    try:
        return re.compile(pattern)

    except re.error:
        print("Somthing is wrong with the regex you have entered please validate it: {regex}".format(regex=pattern))
        exit(1)


def find_matches(lines, pattern):
    """
    searches for matches inside a list containing text by lines and
    :param lines: list of lines to search on
    :param pattern: a compile regex pattern to search in lines
    :returns list by lines containing a list of matches
    """
    try:
        match_list = list()

        for line in lines:
            line_matches = list(re.finditer(pattern, line))

            match_list.append(line_matches)

        return match_list

    except re.error as error:
        print('the script failed because of the following exception: {error}'.format(error=error))
        exit(1)


def read_lines(file, args):
    """
    read the input lines rather a file or stdin and splits tham whith 'readlines()' to linespy
    :param file: the file name you would like to read
    :param args: argparser object containing parameters given to the script
    :returns list containing the lines from the input
    """
    if args.files:
        try:
            with open(file, 'r') as file_handler:
                input_lines = file_handler.readlines()

        except FileNotFoundError:
            print("The file name you entered cannot be found please validate it: {filename}".format(filename=file))
            exit(1)

        except Exception as error:
            print('The following error occurred while reading the file: {error}'.format(error=error))
            exit(1)

        try:
            # Adds a new line in the end of the input
            if input_lines[-1][-1] != '\n':
                input_lines.append('\n')

        except IndexError:
            print('It seems like the file you are trying to search in is empty')

        return input_lines

    elif not sys.__stdin__.isatty():
        try:
            input_lines = sys.stdin.readlines()

        except Exception as error:
            print('The stdin is unreadable for the following reason: {error}'.format(error=error))

        try:
            # Adds a new line in the end of the input
            if input_lines[-1][-1] != '\n':
                input_lines.append('\n')

        except IndexError:
            print('It seems like the input you are trying to search in is empty')

        return input_lines

    else:
        print('It seems like there is no stdin or usage of -f/--files flag')
        exit(1)


def main():
    """entry point"""
    # creating argument parser and mutually exclusive group
    parser = argparse.ArgumentParser(description='This script is a grep like tool (with different arguments) enjoy :)')
    exclusive_group = parser.add_mutually_exclusive_group()

    parser.add_argument('-f', '--files', help='Files you would like to search the regex in seperated with \',\''
                                              ', if no file is passed the script will use stdin insted ')
    parser.add_argument('-r', '--regex', help='A regex to search for in a file or sdtin', required=True)
    exclusive_group.add_argument('-c', '--color', help='Color highlight the match', action='store_true')
    exclusive_group.add_argument('-m', '--machine', help='Generate machine readable output', action='store_true')
    exclusive_group.add_argument('-u', '--underscore', help='Underscores matches', action='store_true')

    args = parser.parse_args()

    pattern = compile_re_pattern(args.regex)

    for file in str(args.files).split(','):
        lines = read_lines(file, args)

        if not args.files:
            file = 'stdin'

        # creates OutputPrinter object instance per file and invokes the functor
        output_printer = OutputPrinter(find_matches(lines, pattern), lines, file, args)
        output_printer()


if __name__ == "__main__":
    main()
