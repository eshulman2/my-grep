import pytest
import re
import argparse
from output_printer import *

def test_positive_color_line():
	line = "some string"
	pattern = re.compile("some")
	line_matches = list(re.finditer(pattern, line))

	assert color_line(line_matches, line) == "\033[91msome\033[0m string"


def test_negative_color_line_matches_empty():
	line = "some string"

	assert color_line([], line) == line


def test_positive_underscore_printer():
	line = "some string"
	pattern = re.compile("string")
	line_matches = list(re.finditer(pattern, line))
	offset = 0
	# 														 "some string"
	assert underscore_printer(offset, line_matches, line) == "     ^^^^^^"


def test_negative_underscore_empty_pattern():
	line = "some string"
	pattern = re.compile("")
	line_matches = list(re.finditer(pattern, line))
	offset = 0
	# 														 "some string"
	assert underscore_printer(offset, line_matches, line) == "           "


def test_OutputPrinter_functor_values(capsys):
	lines = ["some string"]
	file = "file.txt"
	pattern = re.compile("some")
	line_matches = [list(re.finditer(pattern, lines[0]))]
	parser = argparse.ArgumentParser()

	parser.add_argument('-c', '--color', help='Color highlight the match', action='store_true')
	parser.add_argument('-m', '--machine', help='Generate machine readable output', action='store_true')
	parser.add_argument('-u', '--underscore', help='Underscores matches', action='store_true')

	# Machine setting on
	args = parser.parse_args(['-m'])
	OutputPrinter(line_matches, lines, file, args).execute()
	assert "file.txt:1:0:some\n" == capsys.readouterr().out

	# Color setting on
	args = parser.parse_args(['-c'])
	OutputPrinter(line_matches, lines, file, args).execute()
	assert "in file.txt at line 1: \033[91msome\033[0m string" == capsys.readouterr().out

	# Underscore setting on
	args = parser.parse_args(['-u'])
	OutputPrinter(line_matches, lines, file, args).execute()
	assert "in file.txt at line 1: some string" \
		   "                       ^^^^ \n" == capsys.readouterr().out

	# All three are not activated
	args = parser.parse_args([])
	OutputPrinter(line_matches, lines, file, args).execute()
	assert "in file.txt at line 1: some string" == capsys.readouterr().out